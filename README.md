# Face Memory

It's a game to learn to associate a name and a face.

```sh
npm install
npm run all
```

## Tools

* [Outil en ligne image fractionnée](https://splitter.imageonline.co/fr/): practical for the trombinoscope
* [Noms français](https://fr.fantasynamegenerators.com/noms-fran%C3%A7ais.php): french first and last name generator
* [Dawnbringer 16 palette](https://lospec.com/palette-list/dawnbringer-16): color palette used by the application
* [Thispersondoesnotexist](https://www.thispersondoesnotexist.com/): face generator
* [free AI generated photos](https://generated.photos/faces)
