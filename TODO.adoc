= TODO

* [ ] Choisir indépendamment le nom et ou le prénom
* [ ] Mettre un minuteur
* [ ] Pouvoir mettre en pause et cacher la carte
* [ ] Retenir max 10 séries

***

* [x] Le rendre responsive
* [x] Choisir de ne réviser que les erreurs
* [x] Relancer une série
* [x] Nettoyer les données locales
* [x] Stocker localement les erreurs
* [x] Se rappeler de la dernière série
* [x] Mettre un bouton de sauvegarde
* [x] Décorréler l'affichage des vrais statuts. Se baser sur la current value pour afficher le cercle.
* [x] Vérifier que le début et avant la fin sinon popup
* [x] Rédiger un préambule avec les astuces
* [x] Ajouter des infobulles sur les boutons
* [x] Faire en sorte que le max des inputs soit corrélé au JS
* [x] Mettre un bouton réussite échec
* [x] Choisir l'amplitude de la révision et le début

***

* [ ] [.line-through]#Virer les images en erreurs dès qu'elles sont réussies#
* [ ] [.line-through]#Ajouter un bouton découverte#
