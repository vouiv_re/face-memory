"use strict";

import { Series } from "./class/series.js";
import { createElt, createArray } from "./tool.js"

let main = document.getElementById("main");

let dataStorage = JSON.parse(localStorage.getItem("data"));
if (dataStorage === null) {
    localStorage.setItem("data", JSON.stringify([]));
}

let column = ["Date", "Nombres de cartes", "Nombres d'erreurs", "Actions"];
let columnClass = ["date", "nb", "error", "action"];

let deleteBtn = document.getElementById("delete");
deleteBtn.addEventListener("click", () => {
    localStorage.clear();
    document.location.reload();
});

function run() {
    if (dataStorage !== null && dataStorage.length > 0) {
        let trHead = createElt("tr");
        let tbody = createElt("tbody");

        // column.forEach(label => {
        //     trHead.appendChild(createElt("th", label));
        // });
        for (let i = 0; i < column.length; i++) {
            trHead.appendChild(createElt("th", column[i], null, columnClass[i]));            
        }

        dataStorage.slice().reverse().forEach(row => {
            let btns = [];
            let redoBtn = createElt("p", null, null, "action-btn redo", [
                createElt("i", null, null, "fas fa-redo fa-1x")
            ])
            redoBtn.title = "Rejouer la série";
            redoBtn.addEventListener("click", () => {
                redo(row);
            });

            btns.push(redoBtn);

            if (row.errors > 0) {
                let redoErrorsBtn = createElt("p", null, null, "action-btn sync", [
                    createElt("i", null, null, "fas fa-sync fa-1x")
                ])
                redoErrorsBtn.title = "Rejouer les erreurs de la série";
                redoErrorsBtn.addEventListener("click", () => {
                    redoErrors(row);
                });
                btns.push(redoErrorsBtn);
            }

            tbody.appendChild(createElt("tr", null, null, null, [
                createElt("td", row["date"], null, "date"),
                createElt("td", row["end"] - row["begin"] + 1, null, "nb"),
                createElt("td", row["errors"], null, "error"),
                createElt("td", null, null, "action", btns),
            ]))
        });

        main.appendChild(createArray(trHead, tbody, "data"));
    } else {
        main.appendChild(createElt("p", "Aucune série n'est enregistrée localement.", null, "message-none"))
    }
}

function redo(data) {
    let begin = data.begin;
    let end = data.end - data.errors;
    let deck = data.deck.slice(0, data.end - (data.errors + data.begin) + 1);
    for (let i = 0; i < deck.length; i++) {
        deck[i]["status"] = "unread";
    }
    let series = new Series(begin, end, deck, 0, 0);

    localStorage.setItem("next", JSON.stringify(series));
    window.location.href = "../../index.html";
}

function redoErrors(data) {
    let deckOld = data.deck.slice(0, data.end - (data.errors + data.begin) + 1);
    let deck = [];
    deckOld.forEach(row => {
        if (row.status === "lose") {
            row.status = "unread";
            deck.push(row);
        }
    });
    let series = new Series(0, deck.length - 1, deck, 0, 0);
    console.log(series);

    localStorage.setItem("next", JSON.stringify(series));
    window.location.href = "../../index.html";
}

run();