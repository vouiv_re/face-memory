"use strict";

import { createElt, displayPopup, getRandomInt, asyncCall } from "../tool.js";
import { Card } from "./card.js";

let femaleNames = null;
let maleNames = null;

asyncCall(`../../src/json/female.json`)
    .then(data => {
        femaleNames = data;
        asyncCall(`../../src/json/male.json`)
            .then(data1 => {
                maleNames = data1;
            })
    })

let startSection = document.getElementById("start-section");
let card = document.getElementById("card");
let seriesSection = document.getElementById("series");

let dataStorage = JSON.parse(localStorage.getItem("data"));
if (dataStorage === null) {
  localStorage.setItem("data", JSON.stringify([]));
}

export class Series {
  constructor(begin = 0, end = 4, deck = [], currentCard = 0, errors = 0, date = new Date(Date.now()).toLocaleDateString()) {
    this.begin = begin;
    this.end = end;
    this.deck = deck;
    this.currentCard = currentCard;
    this.errors = errors;
    this.date = date;
    // this.time = 0;
  }

  init() {
    if (this.begin < this.end) {
      this.begin = parseInt(this.begin);
      this.end = parseInt(this.end);

      let range = this.end - this.begin + 1;

      let order = [];
      for (let i = 0; i < range; i++) {
        let nb = getRandomInt(this.end, this.begin);
        if (!order.includes(nb)) {
          order.push(nb);
          this.deck.push(new Card(nb));
        } else {
          i--;
        }
      }
      this.displaySeriesSection();
      this.drawCurrentCard();
    } else {
      displayPopup("Le début de série début doit être strictement inférieur à la fin de celle-ci");
    }
  }

  displaySeriesSection(end = false) {
    seriesSection.innerHTML = "";
    startSection.classList.add("hidden");

    for (let i = 0; i < this.deck.length; i++) {
      let bullet = createElt("p", null, `card-${this.deck[i].id}`, `bullet ${this.deck[i].status}`);
      bullet.title = this.deck[i].status;

      if (i === this.currentCard && !end) {
        bullet = createElt("p", null, `card-${this.deck[i].id}`, `bullet current`);
        bullet.title = "current";
      }

      seriesSection.appendChild(bullet);
    }
  }

  drawCurrentCard() {
    card.innerHTML = "";
    
    let currentCard = this.deck[this.currentCard];

    let img = document.createElement("img");
    img.src = `./src/img/${currentCard.gender}/${currentCard.id}.png`;
    img.alt = `Portrait of a ${currentCard.gender}`;
    card.appendChild(img);

    let name = createElt("p", "Révéler", null, "btn reveal");

    name.addEventListener("click", () => {
        name.textContent = (currentCard.gender === "female" ? femaleNames[currentCard.id] : maleNames[currentCard.id]);
        name.classList.remove("reveal");
        name.classList.add("name");

        let validateBtn = createElt("p", null, "validate", "btn validate", [
            createElt("i", null, null, "fas fa-check-circle fa-2x")
        ]);
        let disableBtn = createElt("p", null, "disable", "btn disable", [
            createElt("i", null, null, "fas fa-times-circle fa-2x")
        ]);

        validateBtn.addEventListener("click", () => {
          if (this.deck[this.currentCard].status === "retry") {
            this.deck[this.currentCard].status = "back";
          } else {
            this.deck[this.currentCard].status = "win";
          }
          this.next();
        });

        disableBtn.addEventListener("click", () => {
          if (this.deck[this.currentCard].status === "retry") {
            this.deck[this.currentCard].status = "again";
          } else {
            this.deck[this.currentCard].status = "lose";
          }
          this.deck.push(new Card(currentCard.id, currentCard.gender, "retry"));
          this.end++;
          this.errors++;

          this.next();
        });

        let section = createElt("section", null, null, null, [
            validateBtn,
            disableBtn
        ]);
        card.appendChild(section);
    });
    card.appendChild(name);
  }

  next() {
    if (this.currentCard < this.end - this.begin) {
      this.currentCard++;
      this.displaySeriesSection();
      this.drawCurrentCard();
    } else {
      let message = "";
      if (this.errors === 0) {
        message = createElt("p", `Bravo, vous n'avez fait aucune erreur.`, null, "message");
      } else {
        message = createElt("p", `Vous avez fait ${this.errors} erreur(s).`, null, "message");
      }
      let saveBtn = createElt("p", "Sauvegarder les résultats", "save-btn", "btn blue");
      let endBtn = createElt("p", "Clôturer la série", null, "btn grey");
      saveBtn.addEventListener("click", () => {   
        this.save();
        saveBtn.classList.add("hidden");
      });
      endBtn.addEventListener("click", () => {   
        seriesSection.innerHTML = "";
        card.innerHTML = "";
        startSection.classList.remove("hidden"); 
      });
      card.innerHTML = "";

      card.appendChild(message);
      card.appendChild(saveBtn);
      card.appendChild(endBtn);

      this.displaySeriesSection(true);
    }
  }

  save() {
    dataStorage.push({
      "begin": this.begin,
      "end": this.end,
      "deck": this.deck,
      "currentCard": this.currentCard,
      "errors": this.errors,
      "date": this.date,
    });
    localStorage.setItem("data", JSON.stringify(dataStorage));
  }

  play() {
  }

  break() {
  }
}
