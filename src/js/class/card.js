"use strict";

import { getRandomInt } from "../tool.js";

export class Card {
  constructor(id, gender = getRandomInt(1) % 2 === 0 ? "female" : "male", status = "unread") {
    this.id = id;
    this.gender = gender;
    this.status = status; // unread / current / win / lose / retry / again / back
  }

  updateStatus(status) {
    this.status = status;
  }
}
